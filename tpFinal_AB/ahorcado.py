import random

lista=["Invierno", "Pulsacion"]
palabra_random = random.choice(lista) #se escoge una palabra de la lista

intentos = 6

palabra_usuario = palabra_random[0] #se guarda primer letra palabra random en variable palabra usuario

cant_letras = len(palabra_random) #se guarda en "cant_letras" la cantidad de letras de la palabra random


i=1
while i<cant_letras:    #Rellena espacios de "palabra_usuario" con guiones bajo
    palabra_usuario = palabra_usuario + "_"
    i+=1
    
print(palabra_usuario) #Muestra palabra usuario

n = cant_letras

while palabra_random != palabra_usuario and intentos >=1: #ciclo repetición de intentos

    if intentos >= 1:
            letra = input("Ingresa una letra: ")
            
            while True:
                for i in range(len(palabra_random)):
                 
                    if palabra_random[i] == letra:
                        palabra_usuario = palabra_usuario[0:i] + letra + palabra_usuario[i:]
                    
                        print(palabra_usuario)
                        print("ACERTASTE una letra: ", palabra_usuario[0:n])
                        break
               
                print("No acertaste. Vuelve a intentarlo")
                print("Te quedan ", intentos, "intentos")
                break
    
            
    else:
        print("PERDISTE\nLa palabra era: ", palabra_random)
    
    if palabra_random == palabra_usuario:
        print("¡GANASTE!")
        break
    
        


