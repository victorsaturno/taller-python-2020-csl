"""

nros
48-57

mayúsculas
65-90

minúsculas
97-122

símbolos
33-47 
58-64 
91-96
123-126
161-172
174-400 

Sea x un int , chr(x) devuelve un elemento de la tabla ascii

"""

#!/usr/bin/env python3

import random

def iniciar(longCad):
  while(longCad < 6):
    print("La longitud de la contraseña debe ser como mínimo de 6")
    longCad=int(input("Longitud de la contraseña: "))
  return longCad
  

def distribuirCantidades(cantChars):
  ls=[]
  distr={}
  distr["cantMayus"]=1
  distr["cantMinus"]=1
  distr["cantNros"]=1
  distr["cantSimb"]=1
  for x in range(0,cantChars - 4):
  # se restan 4 porque hay al menos 1 elem de cada tipo de carácter  
  #-----------------------------------
  # 0: representa a las mayúsculas
  # 1: representa a las minúsculas
  # 2: representa a los números
  # 3: representa a los símbolos  
    ls.append(random.randint(0,3))
  distr["cantMayus"]+=ls.count(0)
  distr["cantMinus"]+=ls.count(1)
  distr["cantNros"]+=ls.count(2)
  distr["cantSimb"]+=ls.count(3)
  return distr

def dameLetras(cant,mayus=True):
  ls=[]
  for i in range(0,cant):
    letra=chr(random.randint(97,122))
    if (mayus):
      letra=letra.upper()
    ls.append(letra)
  random.shuffle(ls)	
  return ls
  
def dameNros(cant):
  ls=[]
  for i in range(0,cant):
    ls.append(chr(random.randint(48,57)))
  random.shuffle(ls)	
  return ls

def intervalos2Lista(*rangos):
  res=[]
  for r in rangos :
    res+= list(r)
  random.shuffle(res)	
  return res

def dameSimbolos(cant,intervalo):
  ls=[]
  for i in range(0,cant):
    ls.append(chr(random.choice(intervalo)))
  random.shuffle(ls)
  return ls

def unirIntervalos():
  return intervalos2Lista(range(33,48), range(58,65) , range(91,97), range(123,127) ,range(161,173),range(174,401))

def concatenar(*listas):
    res=[]
    for lista in listas:
        res+= lista
    return res


def main():
    print("La longitud de la contraseña debe ser como mínimo de 6")
    longCad=iniciar(int(input("Longitud de la contraseña: ")))
    dist=distribuirCantidades(longCad)
    juntar= concatenar(dameNros(dist["cantNros"]),dameLetras(dist["cantMinus"],False),dameLetras(dist["cantMayus"]),dameSimbolos(dist["cantSimb"],unirIntervalos()))
    random.shuffle(juntar)
    print(juntar)


#------------------------------------Programa principal----------------------------------------------------

main()