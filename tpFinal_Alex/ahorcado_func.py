import random


def empezarJuego(palabra):
  res=palabra[0]+"\t"
  for i in range(1,len(palabra)):
    res+="_"  
    if(i < len(palabra)-1 ):
      res+="\t"
  return res

def revelar(letra,palabra,revelada):
  res=revelada[0]+ "\t"
  contador=0
  for i in range(1,len(palabra)):
    if (letra == palabra[i] and revelada[i*2] == "_"):
      res+=letra
      contador+=1
    else:
      res+=revelada[i*2]
    if(i<len(palabra)-1):
      res+="\t"
  return {"descifrado":res,"nroDescubiertas":contador}

def mostrarAnotadas(ls):
  res=""
  for i in range(0,len(ls)):
    res+=ls[i]
    if(i != len(ls) - 1):
      res+=","
  print (res if len(res) > 0 else "No se han ingresado letras")


def ingresarLetra(letra,letras):
  while(len(letra) != 1 or letra in letras):
    print("Letras anotadas")
    mostrarAnotadas(letras)
    letra=str.lower(input("Ingrese una letra: "))
  letras.append(letra)
  return letra