#!/usr/bin/env python3

import os
import archivo_func


def main():
    # Me fijo si el archivo existe
    if os.path.isfile("./palabras.txt"):
        print("El archivo"+ " \" palabras.txt \"" +" existe.")
        # Creo que una lista con todas las palabras 
        # existentes(sin el \n) en el archivo 
        agregadas=archivo_func.archivo2Lista()
        archivo_func.agregarPalabra(input("Desea ingresar más palabras?(\"S\" para confirmar): "),agregadas,"a")
    else:
        agregadas=[]
        print("El archivo"+ " \" palabras.txt \"" +" no existe.")
        archivo_func.agregarPalabra(input("Desea ingresar más palabras?(\"S\" para confirmar): "),agregadas,"w")

#-----------------------------Programa principal--------------------------------------

main()