#!/usr/bin/env python3

import random

def armarReglas():
  return {"Piedra": "Tijera", "Papel": "Piedra", "Tijera":"Papel"}

def opciones():
  return ("Piedra","Papel","Tijera")

def jugar(eleccion):
  while (eleccion not in opciones()):
    eleccion = input("Turno del Jugador. \nPiedra,Papel o Tijera: ")
  return eleccion

def cpu():
  return opciones()[random.randint(0,2)]

def main():
    ptosPersona=0
    ptosCpu=0
    rondas=1
    reglas= armarReglas()
    hayGanador = False

    while(not hayGanador):
        print("Ronda:",rondas)
        jugadaCpu = cpu()
        jugadaPersona = jugar(input("Turno del Jugador.\nPiedra,Papel o Tijera: "))
        print("Jugada CPU:",jugadaCpu)
        ptosPersona+= (reglas[jugadaPersona] == jugadaCpu)
        ptosCpu+= (reglas[jugadaPersona] != jugadaCpu and jugadaPersona != jugadaCpu)
          
        print("Puntaje del Jugador:",ptosPersona)
        print("Puntaje de la CPU:",ptosCpu)
        print("+++++++++++++++++++++++++++++")
        hayGanador = rondas >= 3 and abs(ptosCpu - ptosPersona) >= 1
        rondas+= not hayGanador
            
        print("\n")    
        print("=============================")
        print("Rondas jugadas:",rondas)
        print("Puntaje del Jugador:",ptosPersona)
        print("Puntaje de la CPU:",ptosCpu)
        print("El ganador es:", "CPU" if ptosCpu > ptosPersona else "Persona")    


#-----------------------------Programa principal--------------------------------------

main()


