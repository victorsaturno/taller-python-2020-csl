import os

def agregarPalabra(ingresar,agregadas,modo):
    # modo == "a" para agregar al final
    # modo == "w" para pisar el archivo
    
    if (ingresar == "S"):
      archivo=open("./palabras.txt",modo)
      while(ingresar == "S"):
        nueva=input("Nueva palabra: ")
        while(len(nueva) < 3 or nueva in agregadas):
            if (len(nueva) < 3):
                print("La palabra debe tener al menos 3 carácteres.")
            else:
                print("La palabra " + "\""+nueva+"\""+" ya fue ingresada.")
            nueva=input("Nueva palabra: ")
        archivo.write(nueva +"\n")
        agregadas.append(nueva)
        ingresar=input("Desea ingresar más palabras?(\"S\" para confirmar): ")
      archivo.close()

def archivo2Lista():
    # abro el archivo en modo lectura
    archivo=open("./palabras.txt")
    lista=[]
    for linea in archivo.readlines():       
        #strip le saca los \n
        linea=linea.strip()
        if linea != "":
            lista.append(linea) 
    archivo.close()
    return lista

def existeArchivo():
    return os.path.isfile("./palabras.txt")

