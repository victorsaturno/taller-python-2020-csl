#!/usr/bin/env python3

import random
from archivo_func import archivo2Lista,existeArchivo
import ahorcado_func

def main():
    if existeArchivo() and len(archivo2Lista()) > 0 :
        
        intentos=6
        palabras= archivo2Lista()
        palabra=palabras[random.randint(0,len(palabras)-1)]
        revelada=ahorcado_func.empezarJuego(palabra)
        adivinadas=0
        anotadas=[]
        print(revelada)
        gano = adivinadas == len(palabra)-1


        while(intentos > 0 and not gano):
            print("Intentos:",intentos)
            print("Letras anotadas")
            ahorcado_func.mostrarAnotadas(anotadas)
            print("♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦♦")
            letra=ahorcado_func.ingresarLetra(str.lower(input("Ingrese una letra: ")),anotadas)
            jugada=ahorcado_func.revelar(letra,palabra,revelada)
            revelada=jugada["descifrado"]
            adivinadas+=jugada["nroDescubiertas"]
            if (jugada["nroDescubiertas"] == 0) :
                intentos-=1
                print(revelada)
                print("••••••••••••••••••••••••••••••••••••")
                gano = adivinadas == len(palabra)-1
    
        if (gano):
            print("Usted ha ganado")
        else:
            print("Usted ha perdido")
            print("La palabra era",palabra.upper())
    else:
        print("No hay palabras para jugar")
    

#-----------------------------Programa principal--------------------------------------
              
main()
        