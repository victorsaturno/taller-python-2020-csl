#!/usr/bin/env python3

import random

def iniciar(longCad):
  while(longCad < 6):
    print("La longitud de la contraseña debe ser como mínimo de 6")
    longCad=int(input("Longitud de la contraseña: "))
  return longCad


def distribiurCantidades(cantChars):
  distr={}
  distr["cantMayus"]=1
  distr["cantMinus"]=1
  distr["cantNros"]=1
  distr["cantSimb"]=0
  cantChars-=3

  print("Tiene",cantChars,"carácteres para distribuir. Ya hay 1 mayúscula, 1 minúscula,1 número y 0 símbolos.")

  cantNum=int(input("Cuántos números quiere agregar?: "))
  while(cantNum < 0 or cantNum > cantChars):
    cantNum=int(input("Cuántos números quiere agregar?: "))
  distr["cantNros"]+=cantNum  
  cantChars-=cantNum
  

  if (cantChars > 0 ):
    cantLet=int(input("Cuántas letras quiere agregar?: "))
    while (cantLet < 0 or cantLet > cantChars):
      cantLet=int(input("Cuántas letras quiere agregar?: "))
    nroMayus=random.randint(0,cantLet)
    distr["cantMayus"]+=nroMayus
    distr["cantMinus"]+= cantLet-nroMayus
    cantChars-=cantLet
    
  print("\n")  
  print("La contraseña tendrá",nroMayus+1,"letras mayúsculas.")
  print("La contraseña tendrá",cantLet-nroMayus+1,"letras minúsculas.")
  print("La contraseña tendrá",cantNum + 1,"números.")
  print("La contraseña tendrá",cantChars,"símbolos.")  
  distr["cantSimb"]+=cantChars

  return distr


    
def dameLetras(cant,mayus=True):
  ls=[]
  for i in range(0,cant):
    letra=chr(random.randint(97,122))
    if (mayus):
      letra=letra.upper()
    ls.append(letra)
  random.shuffle(ls)	
  return ls


  
def dameNros(cant):
  ls=[]
  for i in range(0,cant):
    ls.append(chr(random.randint(48,57)))
  random.shuffle(ls)	
  return ls

def intervalos2Lista(*rangos):
  res=[]
  for r in rangos :
    res+= list(r)
  random.shuffle(res)	
  return res

def dameSimbolos(cant,intervalo):
  ls=[]
  for i in range(0,cant):
    ls.append(chr(random.choice(intervalo)))
  random.shuffle(ls)
  return ls

def unirIntervalos():
  return intervalos2Lista(range(33,48), range(58,65) , range(91,97), range(123,127) ,range(161,173),range(174,401))

def concatenar(*listas):
    res=[]
    for lista in listas:
        res+= lista
    return res


def main():
    print("La longitud de la contraseña debe ser como mínimo de 6")
    longCad=iniciar(int(input("Longitud de la contraseña: ")))
    dist=distribiurCantidades(longCad)
    juntar= concatenar(dameNros(dist["cantNros"]),dameLetras(dist["cantMinus"],False),dameLetras(dist["cantMayus"]),dameSimbolos(dist["cantSimb"],unirIntervalos()))
    random.shuffle(juntar)
    print("\n La contraseña es...")
    print(juntar)


#-----------------------------Programa principal--------------------------------------

main()