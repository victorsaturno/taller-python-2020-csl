#!/usr/bin/env python3

import random

def quiereJugar(respuesta):
  while(respuesta not in {"S","N"}):
    respuesta=input("Desea jugar?(S/N): ")
  return respuesta

def adivinar(nro):
  while(nro < 0 or nro > 30 ):
    nro = int(input("Diga un número entre 0 y 30: "))
  return nro

def generarNro():
  return random.randint(0,30)

def mostrarPista(generado, jugada):
  if (generado > jugada):
    print("El número elegido -",jugada,"- es MENOR que el generado")
  else:
    print("El número elegido -",jugada,"- es MAYOR que el generado")
  
  print("\n*******************************")


def main():
    seguir=quiereJugar(input("Desea jugar?(S/N): "))
    if (seguir == "S"):
        intentos= 1
        gen = generarNro()
        gano = False
        while(seguir == "S" and not gano):
            print("# Intentos:",intentos)
            jugada=adivinar(int(input("Diga un número entre 0 y 30: ")))
            gano = jugada == gen
            if (not gano):
                intentos+=1
                mostrarPista(gen,jugada)
                seguir= quiereJugar(input("Desea jugar?(S/N): "))
            else:
                print("Usted ha adivinado")


#-----------------------Programa principal--------------------------------

main()

