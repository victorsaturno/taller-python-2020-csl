# Taller Python 2020 - CSL

Proyectos finales del Taller de Python que se dictó en el Club de Software libre

# Trabajo final del taller

Como actividad final del taller les propusimos armar un  pequeño programa, con licencia GPL3

## Generador de contraseñas.

Realizar un programa que genere contraseñas aleatorias.
El programa debe:

- Preguntarle al usuario por la longitud de la contraseña.

-  Mezclar mayúsculas y minúsculas.

- Contener símbolos y números.

- La contraseña resultante debe tener un mínimo de 6 caracteres.

- Opcional: Puede preguntar cuantas letras quiere o cuantos números y si quiere o no símbolos.

> El módulo random puede tener funciones que lo ayuden a desarrollar este programa. Algunas funciones que pueden ser útiles son:
> shuffle() para mezclar los elementos de las listas
> randint() para generar números aleatorios
> choise() para elegir elementos de una lista de manera aleatoria.
> Recuerde que puede hacer uso de las funciones upper() y lower() para trabajar con strings.

## Adivinar un número.
Realizar un programa en el cual la computadora elija un número entre 0 a 30, y luego le pida al usuario adivinarlo.
El programa debe:

- Elegir un número al azar entre 0 y 30 (inclusive)
- Preguntarle al usuario cual es su número elegido.
- Si acierta debe informar que el usuario ganó el juego
- Si no acierta debe informar en pantalla si el número que el usuario eligió es mayor o menor que el número a adivinar.
- Debe informar cuantos intentos tomó acertar.

  > El módulo random puede tener funciones que lo ayuden a desarrollar este programa.
  > Puede ser útil:
  > randint() para generar números aleatorios.
  > Recuerde hacer uso de las estructuras de control if y while para hacer las comparaciones.



## Juego del ahorcado
El programa debe elegir una palabra al azar y el usuario tiene 6 intentos para adivinarla. Si no lo hace pierde el juego.
El programa debe:
+ Elegir una palabra entre varias palabras predefinidas.

+ Mostrar en pantalla la primer letra de la palabra y el resto de las letras con guion bajo. Ejemplo: 

+ Si el programa seleccionó la palabra "Celular" debe mostrar en pantalla "C__"

+ Pedirle al usuario que ingrese una letra

+ Si acertó, debe dibujarse esa letra (en pantalla) en la posición que corresponda . Caso contrario restarle un intento.

+ El programa debe chequear que solo se ingresen caracteres.

+ Opcional: El programa puede leer una lista de palabras desde un archivo de texto plano.

> El módulo random puede tener funciones que lo ayuden a desarrollar este programa.
> Algunas funciones que pueden ser útiles son:
> shuffle() para mezclar los elementos de las listas
> randint() para generar números aleatorios.
> choise()** para elegir elementos de una lista de manera aleatoria.
> Recuerde hacer uso de las estructuras de control if, while y for para hacer las comparaciones o recorrer estructuras de datos.



## Piedra, papel o tijera
El programa debe elegir "Piedra", "Papel" o "Tijera", el usuario lo mismo.
Luego se compara bajo la siguiente tabla:

------

Piedra le gana a Tijera.

Papel le gana a Piedra

Tijera le gana a Papel.

------

Si ambos eligen lo mismo empatan.
Se juegan 3 rondas, gana el que obtenga mejor puntaje, si han empatado juego una nueva ronda hasta que exista un ganador.

> El módulo random puede tener funciones que lo ayuden a desarrollar este programa.
> Algunas funciones que pueden ser útiles son
> shuffle() para mezclar los elementos de las listas
> randint() para generar números aleatorios.
> choise() para elegir elementos de una lista de manera aleatoria.
> Recuerde hacer uso de las estructuras de control if y while para hacer las comparaciones.