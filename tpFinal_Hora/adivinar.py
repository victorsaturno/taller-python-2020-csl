# -*- coding: utf-8 -*-
#ADIVINADOR
import random

num = random.randint(0,30)
valor = -1
intentos = 0

while valor != num:
       intentos +=1
       print ("Introduzca un número entre 0 y 30:\n")       
       valor = input("Ingrese su opción: ")
       valor = int(valor)
       if(valor<num):
            print ("El número ingresado es menor\n") 
       elif(valor>num):           
           print ("El número ingresado es mayor\n")

print("Acertó, el número elegido era", num)
print("Tardó",intentos,"intentos en acertar")